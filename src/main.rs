extern crate notify;

use notify::{watcher, DebouncedEvent, RecursiveMode, Watcher};
use std::fs;
use std::path::PathBuf;
use std::sync::mpsc::channel;
use std::time::Duration;

struct BaseFile<'a> {
    path: &'a PathBuf,
}

impl<'a> BaseFile<'a> {
    fn move_file(&self, directory_name: &str) {
        let goto = format!(
            "/Users/malo/docs/{}/{}",
            directory_name,
            self.path.file_name().unwrap().to_str().unwrap()
        );

        fs::rename(&self.path, goto).unwrap_or_else(|e| println!("{e}"));
    }
}

fn handle_extension(path: PathBuf) {
    let base_file = BaseFile { path: &path };
    let extension = path.extension().unwrap();

    match extension.to_str().unwrap() {
        "jpg" | "jpeg" | "png" => base_file.move_file("images"),
        "pdf" => base_file.move_file("pdf"),

        _ => (),
    }
}

fn handle_download(event: DebouncedEvent) {
    match event {
        DebouncedEvent::Create(buffer) => {
            handle_extension(buffer);
        }
        _ => (),
    }
}

fn main() {
    let (tx, rx) = channel();

    let mut watcher = watcher(tx, Duration::from_secs(2)).unwrap();

    watcher
        .watch("/Users/malo/Downloads", RecursiveMode::Recursive)
        .unwrap();

    loop {
        match rx.recv() {
            Ok(event) => {
                handle_download(event);
            }

            Err(e) => println!("watch error: {:?}", e),
        }
    }
}
